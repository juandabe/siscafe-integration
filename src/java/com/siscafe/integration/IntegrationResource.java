/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siscafe.integration;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author asistentetic
 */
@Path("integration")
public class IntegrationResource {


    /**
     * Creates a new instance of IntegrationResource
     */
    public IntegrationResource() {
    }

    /**
     * Retrieves representation of an instance of com.siscafe.integration.IntegrationResource
     * @param cargolotid
     * @param weight
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getWeightByCargoLotId")
    public String getWeightByCargoLotId(@QueryParam("cargolotid") String cargolotid,
            @QueryParam("weight") String weight) {
        IntegrationManager integration = new IntegrationManager();
        return integration.weightByCargoLotId(cargolotid, weight);
    }
    
    /**
     * Retrieves representation of an instance of com.siscafe.integration.IntegrationResource
     * @param container
     * @param weight
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getWeightByContainer")
    public String getWeightByContainer(@QueryParam("container") String container,
            @QueryParam("weight") String weight) {
        IntegrationManager integration = new IntegrationManager();
        return integration.weightByContainer(container, weight);
    }
}
