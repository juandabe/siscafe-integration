/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siscafe.integration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author asistentetic
 */
public class IntegrationManager {
    
    public IntegrationManager(){}
    
    private String printSOAPResponse(SOAPMessage soapResponse) throws Exception
    {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        ByteArrayOutputStream writer = new ByteArrayOutputStream ();
        StreamResult result = new StreamResult(writer);
        transformer.transform(sourceContent, result);
        String xmlString = new String(writer.toByteArray(),"UTF-8");
        return xmlString;
    }
    
    public String weightByCargoLotId(String cargoLotId, String weight){
        String result="";
        try{
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            String url = "http://n4.sprbun.com:10080/apex/services/argoservice?wsdl";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequestWeightByCargoLotId(cargoLotId, weight), url);
            result = printSOAPResponse(soapResponse);
            soapConnection.close();
        }
        catch (Exception e)
        {
            System.err.println("Error mientras se enviaba solicitud al Servidor N4");
            e.printStackTrace();
        }
        return result;
    }
    
    public String weightByContainer(String container, String weight){
        String result="";
        try{
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            String url = "http://n4.sprbun.com:10080/apex/services/argoservice?wsdl";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequestWeightByContainer(container, weight), url);
            result = printSOAPResponse(soapResponse);
            soapConnection.close();
        }
        catch (Exception e)
        {
            System.err.println("Error mientras se enviaba solicitud al Servidor N4");
            e.printStackTrace();
        }
        return result;
    }
    
    public SOAPMessage createSOAPRequestWeightByContainer(String container, String weight) throws Exception{
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        
        MimeHeaders hd = soapMessage.getMimeHeaders();
        //hd.addHeader("Authorization", "Basic YWRtaW46YWRtaW4=");//servidor interno copc:copc*17 | servidor publico admin:admin
        hd.addHeader("Authorization", "Basic bjRhcGk6bG9va2l0dXA="); // n4api:lookitup
        
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("arg", "http://www.navis.com/services/argoservice");
        envelope.addNamespaceDeclaration("v1", "http://types.webservice.argo.navis.com/v1.0");
        
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("genericInvoke", "arg");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("scopeCoordinateIdsWsType", "arg");
        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement("operatorId", "v1");
        soapBodyElem2.addTextNode("SPRBUN");
        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement("complexId", "v1");
        soapBodyElem3.addTextNode("COBUN");
        SOAPElement soapBodyElem4 = soapBodyElem1.addChildElement("facilityId", "v1");
        soapBodyElem4.addTextNode("FABUN");
        SOAPElement soapBodyElem5 = soapBodyElem1.addChildElement("yardId", "v1");
        soapBodyElem5.addTextNode("FABUN");
        SOAPElement soapBodyElem6 = soapBodyElem1.addChildElement("externalUserId", "v1");
        soapBodyElem6.addTextNode("");
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("xmlDoc", "arg");

        soapBodyElem7.addTextNode("<![CDATA[\n" +
        "<custom class=\"SPBScaleIntegration3rdPartyWebService\" type=\"extension\">\n" +
        "<container>\n" +
        "<id>"+container+"</id>\n" +
        "<weight>"+weight+"</weight>\n" +
        "<source>COPC</source>\n" +
        "</container>\n" +
        "</custom>\n" +
        "]]>"
        );
        
        soapMessage.saveChanges();
        ByteArrayOutputStream  writer = new ByteArrayOutputStream ();
        soapMessage.writeTo(writer);
        return soapMessage;
    }
    
    public SOAPMessage createSOAPRequestWeightByCargoLotId(String cargoLotId, String weight)  throws Exception{
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        
        MimeHeaders hd = soapMessage.getMimeHeaders();
        //hd.addHeader("Authorization", "Basic YWRtaW46YWRtaW4=");//servidor interno copc:copc*17 | servidor publico admin:admin
        hd.addHeader("Authorization", "Basic bjRhcGk6bG9va2l0dXA="); // n4api:lookitup
        
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("arg", "http://www.navis.com/services/argoservice");
        envelope.addNamespaceDeclaration("v1", "http://types.webservice.argo.navis.com/v1.0");
        
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("genericInvoke", "arg");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("scopeCoordinateIdsWsType", "arg");
        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement("operatorId", "v1");
        soapBodyElem2.addTextNode("SPRBUN");
        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement("complexId", "v1");
        soapBodyElem3.addTextNode("COBUN");
        SOAPElement soapBodyElem4 = soapBodyElem1.addChildElement("facilityId", "v1");
        soapBodyElem4.addTextNode("FABUN");
        SOAPElement soapBodyElem5 = soapBodyElem1.addChildElement("yardId", "v1");
        soapBodyElem5.addTextNode("FABUN");
        SOAPElement soapBodyElem6 = soapBodyElem1.addChildElement("externalUserId", "v1");
        soapBodyElem6.addTextNode("");
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("xmlDoc", "arg");

        soapBodyElem7.addTextNode("<![CDATA[\n" +
        "<custom class=\"SPBUpdateWeightFromCFSWebService\" type=\"extension\">\n" +
        "<cargo-lot>\n" +
        "    <net-weight>"+weight+"</net-weight>\n" +
        "    <cargo-lot-id>"+cargoLotId+"</cargo-lot-id>\n" +
        "</cargo-lot>\n" +
        "</custom>"+
        "]]>"
        );
        
        soapMessage.saveChanges();
        ByteArrayOutputStream  writer = new ByteArrayOutputStream ();
        soapMessage.writeTo(writer);
        return soapMessage;
    }
}
